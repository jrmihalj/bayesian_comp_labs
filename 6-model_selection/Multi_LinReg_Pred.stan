
data {
  
  int n_train;
  int n_withhold;
  int n_input;
  
  real y_vec[n_train];
  
  matrix[n_train, n_input] x_mat;
  matrix[n_withhold, n_input] x_pred;
  
}


parameters {
  
  vector[n_input] beta;
  real alpha;
  real<lower = 0> sigma;
  
}

transformed parameters{
  
  vector[n_train] mu;
  vector[n_withhold] mu_pred;
  
  // This is using matrix algebra.
  // Think about how to write this out long-hand:
  mu = alpha + x_mat * beta; 
  mu_pred = alpha + x_pred * beta; 
  
}

model {
  
  // PRIORS:
  alpha ~ normal(0, 50);
  
  for(i in 1:n_input){
    beta[i] ~ normal(0, 50);
  }
  
  sigma ~ cauchy(0, 2.5);
  
  
  // DATA LIKELIHOOD
  
  y_vec ~ normal(mu, sigma);
}

generated quantities{
  
  real log_lik[n_train];
  real y_pred[n_withhold];
  
  // Pointwise log likelihood
  for(i in 1:n_train){
    log_lik[i] = normal_lpdf(y_vec[i] | mu[i], sigma);
  }
  
  // Predicted values of y
  for(i in 1:n_withhold){
    y_pred[i] = normal_rng(mu_pred[i], sigma);
  }
  
  
}
