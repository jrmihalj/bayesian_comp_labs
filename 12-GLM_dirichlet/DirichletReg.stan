
data {

  int n_group;
  int n_sample;
  matrix[n_sample, n_group] y_mat;
  real x_vec[n_sample];
  
}


parameters {
  
  vector[n_group-1] beta_raw;
  vector[n_group-1] z_raw;
  real theta;
  
}

transformed parameters{

  matrix[n_sample, n_group] etas;
  matrix[n_sample, n_group] mus;
  matrix[n_sample, n_group] alphas;  
  
  vector[n_group] beta_full;
  vector[n_group] z_full;
  
  for(g in 1:n_group-1){
    beta_full[g] = beta_raw[g];
    z_full[g] = z_raw[g];
  }
  
  beta_full[n_group] = 0;
  z_full[n_group] = 0;
  
  for(n in 1:n_sample){
    for(g in 1:n_group){
      etas[n, g] = z_full[g] + beta_full[g] * x_vec[n];
    }
  }
  
  for(n in 1:n_sample){
    mus[n, ] = transpose(softmax(transpose(etas[n, ])));
  }
  
  for(n in 1:n_sample){
    alphas[n, ] = mus[n, ] * exp(theta);
  }
  
  
}

model {

  // PRIORS:
  beta_raw ~ normal(0, 5);
  z_raw ~ normal(0, 5);
  theta ~ normal(0, 5);
  
  // DATA LIKELIHOOD
  for(n in 1:n_sample){
    
    transpose(y_mat[n, ]) ~ dirichlet(transpose(alphas[n,]));
    
  }
  
}

