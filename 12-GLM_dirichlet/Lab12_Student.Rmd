---
title: "Lab 12"
author: "Your Name"
date: ''
output:
  pdf_document: 
    fig_caption: yes
header-includes:
  \usepackage{float}
  \floatplacement{figure}{H}
  \usepackage{hyperref}
  \hypersetup{
    colorlinks=true,    
    urlcolor=cyan
  }
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.pos = 'h')
```

# Dirichlet Regression in Stan

Load the following libraries. You may have to install the \texttt{Compositional} package. 
```{r, message=FALSE, warning=FALSE}
library(Compositional)
library(tidyverse)
library(rstan)
rstan_options(auto_write = TRUE)
options(mc.cores = parallel::detectCores())
```


## Task 1 (5 points)

1. Import the data set \texttt{species\_composition.csv}. The data set shows how the composition of a plant community changes with nitrogen levels. 

2. Create scatterplots of each group's composition $y_c$ versus the input variable, nitrogen, $x$. It would be best to use the \texttt{par()} function so that all of your plots show up in the same area. 

## Task 2 (15 points)

1. Use Stan to fit a Dirichlet regression to these data. I have provided you with the model statement \texttt{DirichletReg.stan}. Your real task is to understand what that code is doing and why it is set up in this way. 

2. Then, you need to devlop code to run the model, as you have in previous labs. Be sure to verify that the model has converged, etc, \textit{but I do not need to see these convergence diagnostics}. The following code snippets will be useful. 
```{r eval = FALSE}

params_monitor = c("beta_full", "z_full", "alphas")
summary(model_fit)$summary[1:8,]

```
3. Based on the estimates of group-specific slopes, $\beta_c$, briefly interpret how nitrogen affects the composition of this plant community. Which species are affected by nitrogen and how? 

\newpage
# Task 3 (10 points)

1. Get the following code to work for your output. In other words, you may have to change some of the object names. 

2. Comment on each line of code below to explain what's going on. Make sure that your comments do not run over the page width when you compile your code (i.e., use new lines when appropriate). 

3. In general, explain what the plot is generating and why this procedure is useful. Use specific language from the previous lab. 

```{r eval = FALSE}

n_boot = 100
y_fake = array(0, dim = c(n_sample, n_group, n_boot))

for(i in 1:n_boot){
  temp_idx = sample(1:3000, 1)
  alphas_temp = model_out$alphas[temp_idx, , ]
  
  for(n in 1:n_sample){
    y_fake[n, ,i] = rdiri(1, alphas_temp[n, ])
  }
  
}


this_group = 4
plot(NA,NA, 
     # Change the ylim depending on the selected group:
     ylim = c(0,0.2), 
     xlim = c(-1,1),
     xlab = "Nitrogen", ylab = paste0("Proportion of Species ", this_group))

for(i in 1:n_boot){
  points(y_fake[, this_group, i] ~ data_df$Nitrogen_Scaled,
         pch = 19, col="red")
}
  
points(data_df[,this_group] ~ data_df$Nitrogen_Scaled,
       pch = 19, col = "black")

```

# Task (4 points)

Using the posterior samples stored in \texttt{z\_full}, calculate the median expected value of $y_c$ for each group when $x = 0$. Remember that the value of each group will depend on the values of all the other groups. 

