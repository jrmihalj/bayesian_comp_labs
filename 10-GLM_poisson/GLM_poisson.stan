
data {
  // Specify all of your data inputs in this block
  
  int n_sample;
  int y_vec[n_sample];
  real x_vec[n_sample];
  
}


parameters {
  // Here, you specify the parameters that will be estimated from your data
  real beta;
  real alpha;
}

transformed parameters{
  // In this block, you can specify calculations that use only parameters or that 
  // use both parameters and data (e.g., a parameter multipled by a data vector).
  // Conducting such calculations in this block can make your model run faster. 
  
  real l_lambda[n_sample];
  real lambda[n_sample];
  
  for(i in 1:n_sample){
    l_lambda[i] = beta * x_vec[i] + alpha;
  }
  
  lambda = exp(l_lambda);
  
}

model {
  // Here, you specify your parameter priors and your model likelihood
  
  // PRIORS:
  alpha ~ normal(0, 50);
  beta ~ normal(0, 50);

  // DATA LIKELIHOOD
  
  for(i in 1:n_sample){
    y_vec[i] ~ poisson(lambda[i]);
  }
  
}

generated quantities{
  
  real log_lik[n_sample];
  
  for(i in 1:n_sample){
    log_lik[i] = poisson_lpmf(y_vec[i] | lambda[i]);
  }
  
  
}

