---
title: "Lab 13"
author: "Your Name"
date: ''
output:
  pdf_document: 
    fig_caption: yes
header-includes:
  \usepackage{float}
  \floatplacement{figure}{H}
  \usepackage{hyperref}
  \hypersetup{
    colorlinks=true,    
    urlcolor=cyan
  }
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.pos = 'h')
```

# Variable selection in RStanARM with predictive projection

## Task 1 (20 points)

Your task is to simulate data following the assumptions of linear regression, but with many inputs. Complete the following in *one code chunk*. 

1. Assign how many total input variables you want (e.g., \texttt{n\_input = 20}). Remember, the more inputs you have, the longer your model will take to estimate. 

2. Assign how many inputs you want to have non-zero slopes (e.g. \texttt{n\_sig\_input = 5}). This is the number of ``important'' variables. 

3. Randomly assign slopes based on the previous two specifications. You will need as many slopes as you have input variables. The non-significant slopes can be set to 0 or set to a value very close to zero (e.g. $\beta_j \sim N(0.001, 0.01)$).

4. Randomly draw the other required parameters.

5. Create an $x$ matrix and properly center and scale. Remember, each $x$ input is centered and scaled separately. 

6. Calculate a vector of $y$ based on the model parameters and the $x$ matrix. 

7. Save your data properly in a data frame, so that RStanARM can read it (see code from lecture).

## Task 2 (5 points)

Run the linear regression in RStanARM. Follow the code from class to create two plots: (1) with \texttt{varsel\_plot()} and (2) with \texttt{proj\_linpred()}.

Note that the following code helps you decide how many variables to keep:
```{r eval = FALSE}

suggest_size(var_selection, alpha = 0.95, pct = 0.05)

```

## Task 3 (10 points)

Develop a question about this variable selection method. For instance, in class I asked, ``how does changing the $p0$ parameter alter the outcome of variable selection?''. Tell me your question, and conduct a *simple* experiment to address your question. Briefly report your findings. You do not have to conduct a rigorous experiment to answer this question, but show me that you thought through the problem. 
